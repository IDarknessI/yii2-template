<?php
use dmstr\widgets\Alert;

/* @var string $content */
?>

<div class="content-wrapper">
    <section class="content-header visible-xs">
        <?= \yii\widgets\Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'options' => ['style' => ['margin-right' => '0px', 'display' => 'block'], 'class' => 'breadcrumb'],
            ]
        ) ?>
        <div class="clearfix"></div>
    </section>
    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <?= Yii::$app->name ?>
</footer>