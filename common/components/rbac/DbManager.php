<?php

namespace common\components\rbac;

use yii\db\Query;

class DbManager extends \yii\rbac\DbManager
{

    private static $_assignments = [];

    /**
     * @inheritdoc
     */
    public function getAssignments($userId)
    {
        if (empty($userId)) {
            return [];
        }


        if(isset(self::$_assignments[$userId]))
        {
            return self::$_assignments[$userId];
        }

        $query = (new Query)
            ->from($this->assignmentTable)
            ->where(['user_id' => (string)$userId]);

        $assignments = [];
        foreach ($query->all($this->db) as $row) {
            $assignments[$row['item_name']] = new Assignment([
                'userId' => $row['user_id'],
                'roleName' => $row['item_name'],
                'createdAt' => $row['created_at'],
            ]);
        }

        self::$_assignments[$userId] = $assignments;

        return $assignments;
    }
}